//DBに接続
var MongoClient = require('mongodb').MongoClient,
    settings = require('./settings');

var lessons;

MongoClient.connect("mongodb://"+settings.host+"/"+settings.db, function(err,db){
    lessons = db.collection("lessons");
});

//レッスン一覧表示
exports.index = function(req, res) {
    lessons.find().toArray(function(err, items){
        res.send(items);
    });
    //res.render('lesson/index');
}

exports.take = function(req, res) {
    var lessonNum = Number(req.params.id);
    res.send(lessonNum);
//    lessons.find({lessonId:1}).toArray(function(err, item) {
//        res.send(item);
//    });
    //res.render('lesson/take',{lesson:lessons[req.params.id]});
}