// nodetsukutta
var express = require('express'),
    bodyParser = require('body-parser'),
    methodOverride = require("method-override"),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    csrf = require('csurf'),
    path = require('path'),
    app = express(),
    MongoClient = require('mongodb').MongoClient,
    mongodb = require('mongodb'),
    settings = require('./routes/settings'),
    lesson = require('./routes/lesson'); 

//コレクションlessonsのオブジェクト作成
var lessons;

MongoClient.connect("mongodb://"+settings.host+"/"+settings.db, function(err,db){
    lessons = db.collection("lessons");
});

app.set('views', __dirname+'/views');
app.set('view engine', 'ejs');

//middleware
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser());
app.use(methodOverride('_method'));

//csfr対策
app.use(cookieParser());
app.use(session({secret: 'kahs739jd72'}));
app.use(csrf());
app.use(function(req, res, next){
    res.locals.csrftoken = req.csrfToken();
    next();
});

app.get('/',function(req, res) {
//レッスン一覧情報取得
//    lessons.find().toArray(function(err, items){
//        res.send(items);
//    });
    res.render('lesson/index');
});

app.get('/lesson/take/:id([0-9]+)',function(req, res) {
    
    var stream = lessons.find({lessonId:Number(req.params.id)}).stream();
    stream.on("data", function(item) {
//        res.sendStatus(item.lessonTitle);
        console.log(item.lessonId);
        res.render('lesson/take',{lesson:item});
    });
});

/*
app.get('/lesson/take/:id([0-9]+)',lesson.take);
app.get('/posts/:id([0-9]+)', post.show);
app.get('/posts/new',post.new);
app.post('/posts/create',post.create);
app.get('/posts/:id/edit', post.edit);
app.put('/posts/:id', post.update);
app.delete('/posts/:id', post.destroy);
*/

//my_middlewere
app.use(function(err, req, res, next){
    res.send(err.message);
});

app.listen(3000);
console.log("server starting...");

